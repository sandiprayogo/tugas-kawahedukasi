import java.util.Scanner;

class DaftarMenu {
  public static void main(String args[]) {

    Scanner input = new Scanner(System.in);

    int pilihan, angkaAwal, angkaDua;
    char ulang;

    do {
      System.out.println("##  Daftar Menu ##");
      System.out.println("============================");
      System.out.println("1. identitas");
      System.out.println("2. kalkulator");
      System.out.println("3. perbandingan");
      System.out.println();

      System.out.print("Pilihan anda: ");
      pilihan = input.nextInt();

      switch (pilihan) {
        case 1:
          System.out.println("Programer");
          System.out.println("nama: SANDI PRAYOGO");
          System.out.println("alasan di backend: belajar backend dan jadi backend programer");
          System.out.println("ekspektasi: harapan saya menjadi seorang backend programer handal ");
          break;
        case 2:
          do {
            System.out.println("##  kalkulator Menu ##");
            System.out.println("============================");
            System.out.println("1. penjumlahan");
            System.out.println("2. pengurangan");
            System.out.println("3. perkalian");
            System.out.println("4. pembagian");
            System.out.println("5. sisa bagi");
            System.out.println();

            System.out.print("Pilihan anda: ");
            pilihan = input.nextInt();

            switch (pilihan) {
              case 1:
                System.out.println("penjumlahan");
                System.out.print("angka1: ");
                angkaAwal = input.nextInt();
                System.out.print("angka2: ");
                angkaDua = input.nextInt();
                System.out.println("");
                int hasilJumlah = (angkaAwal + angkaDua);
                System.out.println("hasil penjumlahan: " + hasilJumlah);
                break;
              case 2:
                System.out.println("pengurangan");
                System.out.print("angka1: ");
                angkaAwal = input.nextInt();
                System.out.print("angka2: ");
                angkaDua = input.nextInt();
                System.out.println("");
                int hasilKurang = (angkaAwal - angkaDua);
                System.out.println("hasil pengurangan: " + hasilKurang);
                break;
              case 3:
                System.out.println("perkalian");
                System.out.print("angka1: ");
                angkaAwal = input.nextInt();
                System.out.print("angka2: ");
                angkaDua = input.nextInt();
                System.out.println("");
                int hasilKali = (angkaAwal * angkaDua);
                System.out.println("hasil penjumlahan: " + hasilKali);
                break;
              case 4:
                System.out.println("pembagian");
                System.out.print("angka1: ");
                angkaAwal = input.nextInt();
                System.out.print("angka2: ");
                angkaDua = input.nextInt();
                System.out.println("");
                int hasilBagi = (angkaAwal / angkaDua);
                System.out.println("hasil pembagian: " + hasilBagi);
                break;
              case 5:
                System.out.println("sisa bagi");
                System.out.print("angka1: ");
                angkaAwal = input.nextInt();
                System.out.print("angka2: ");
                angkaDua = input.nextInt();
                System.out.println("");
                int hasil = (angkaAwal % angkaDua);
                System.out.println("hasil sisabagi: " + hasil);
                break;
              default:
                System.out.println("Menu tidak tersedia");
            }

            System.out.println();

            System.out.print("Ingin memilih menu lain (y/t)? ");
            ulang = input.next().charAt(0);

            System.out.println();
          } while (ulang != 't');
          break;
        case 3:
          System.out.println("perbandingan");
          System.out.print("angka1: ");
          angkaAwal = input.nextInt();
          System.out.print("angka2: ");
          angkaDua = input.nextInt();
          System.out.println("");
          if (angkaAwal > angkaDua) {
            System.out.println(angkaAwal + "lebih besar dari" + angkaDua);
          } else if (angkaAwal < angkaDua) {
            System.out.println(angkaAwal + "lebih kecil dari" + angkaDua);
          } else {
            System.out.println(angkaAwal + "sama dengan" + angkaDua);
          }
          break;
        default:
          System.out.println("Menu tidak tersedia");
      }

      System.out.println();

      System.out.print("Ingin memilih menu lain (y/t)? ");
      ulang = input.next().charAt(0);

      System.out.println();
    } while (ulang != 't');

    System.out.println("Terimakasih...");

  }
}