package org.kawah.edukasi;

import java.util.ArrayList;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.kawah.edukasi.model.User;

@Path("/users")
public class ControllerResource {

    ArrayList<User> users = new ArrayList<User>();

    Integer id = 0;

    @Inject
    Validator validator;

    // mendapatkan seluruh data user
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<User> getUsers() {
        return users;
    }

    // meendapatkan data user sesuai dengan id
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getUser(int id) {
        for (User user : users) {
            if (user.id == id) {
                return user;
            }
        }
        return "Data dengan id " + id + " tidak ditemukan";
    }

    // menambahkan data user
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Result addUser(User user) {
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        if (violations.isEmpty()) {
            user.id = ++id;
            users.add(user);
            return new Result("Data user berhasil ditambahkan");
        } else {
            return new Result(violations);
        }
    }

    // mengubah data user
    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Result updateUser(int id, User user) {
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        if (violations.isEmpty()) {
            for (User u : users) {
                if (u.id == id) {
                    u.name = user.name;
                    u.email = user.email;
                    u.age = user.age;
                    u.address = user.address;
                    return new Result("Data user berhasil diupdate");
                }
            }
            return new Result("Data dengan id " + id + " tidak ditemukan");
        } else {
            return new Result(violations);
        }
    }

    // menghapus data user sesuai id
    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Object deleteUser(int id) {
        for (User user : users) {
            if (user.id == id) {
                users.remove(user);
                return new Result("Data " + user.name + " berhasil dihapus");
            }
        }
        return new Result("Data dengan id " + id + " tidak ditemukan");
    }

    // menghapus semua data user
    @DELETE
    @Path("/clear")
    @Produces(MediaType.APPLICATION_JSON)
    public Result deleteAllUser() {
        users.clear();
        id = 0;
        return new Result("Semua data user berhasil dihapus");
    }

}
