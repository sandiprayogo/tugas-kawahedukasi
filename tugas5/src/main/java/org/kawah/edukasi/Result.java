package org.kawah.edukasi;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;

// fungsi untuk menampilkan hasil validasi
public class Result {
    private String message;
    private boolean success;

    Result(String message) {
        this.message = message;
        this.success = true;
    }

    Result(Set<? extends ConstraintViolation<?>> violations) {
        this.success = false;
        this.message = violations.stream()
                .map(cv -> cv.getMessage())
                .collect(Collectors.joining(", "));
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }
}
