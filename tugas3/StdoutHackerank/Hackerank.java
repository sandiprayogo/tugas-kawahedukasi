import java.util.Scanner;

public class Hackerank {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int b = 1;
        while (scanner.hasNext()) {
            String a = scanner.nextLine();
            System.out.println(b + " " + a);
            b++;
        }
        scanner.close();
    }
}