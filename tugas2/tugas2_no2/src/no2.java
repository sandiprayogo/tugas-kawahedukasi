import java.util.Scanner;

public class no2 {

    static void header(String header) {
        System.out.println(header);
    }

    static int max(int data[]) {
        int min = data[0];
        int max;
        for (int i = 0; i < data.length; i++)
            if (data[i] > min)
                max = data[i];
        return max;
    }

    public static void main(String[] args) {
        Scanner masukan = new Scanner(System.in);
        header("Program Pencari Maks");
        int data[] = new int[0];
        System.out.print("Masukkan jumlah data: ");
        int jmlh = masukan.nextInt();
        data = new int[jmlh];
        for (int i = 0; i < data.length; i++) {
            System.out.print("Masukkan data ke-" + (i + 1) + ": ");
            data[i] = masukan.nextInt();
        }

        System.out.println("Nilai maksimum = " + max(data));
    }
}