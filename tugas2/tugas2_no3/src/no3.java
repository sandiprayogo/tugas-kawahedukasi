import java.util.Scanner;

public class no3 {

    static void header(String header) {
        System.out.println(header);
    }

    static int pangkat(int bil, int pangkat) {
        if (pangkat == 0)
            return 1;
        else
            return bil * pangkat(bil, pangkat - 1);
    }

    public static void main(String[] args) {
        Scanner masukan = new Scanner(System.in);
        header("Pangkat Rekursif");
        System.out.print("Input bil: ");
        int bil = masukan.nextInt();
        System.out.print("Input pangkat: ");
        int pangkat = masukan.nextInt();
        int hasil = pangkat(bil, pangkat);
        System.out.print("Hasil = " + hasil);
        System.out.println("");
    }
}