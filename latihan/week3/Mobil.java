package week3;

public class Mobil extends Kendaraan implements Supir {

    public Mobil(int roda) {
        super(roda);
    }

    public void tancapGas() {
        System.out.println("Ngueeeng");
    }

    public void nyatir() {
        System.out.println("Nyatir");
    }
}
